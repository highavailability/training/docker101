Example Voting App
=========

A simple distributed application running across multiple Docker containers.

Getting started
---------------

If you want to work with the files directly in the repository - and you haven't already cloned the repo. Follow these steps for LAB03:

```bash
$ cd ~
$ git clone https://gitlab.com/highavailability/training/docker101.git
$ cd ~/docker101/lab03
```



## Linux Containers

The Linux stack uses Python, Node.js, .NET Core (or optionally Java), with Redis for messaging and Postgres for storage.


Run in this directory:
```
docker-compose up -d
```
The app will be running at [http://10.234.5.x:5000](http://10.234.5.x:5000), and the results will be at [http://10.234.5.x:5001](http://10.234.5.x:5001).

Run some docker-compose commands to look at the environment

```bash
docker-compose ps
docker-compose logs
docker-compose top
docker-compose images
```

When your done - feel free to bring the apps down! No RUSH - take your time and look around.

```bash
   docker-compose down
```

Architecture
-----

![Architecture diagram](architecture.png)

* A front-end web app in [Python](/vote) or [ASP.NET Core](/vote/dotnet) which lets you vote between two options
* A [Redis](https://hub.docker.com/_/redis/) or [NATS](https://hub.docker.com/_/nats/) queue which collects new votes
* A [.NET Core](/worker/src/Worker), [Java](/worker/src/main) or [.NET Core 2.1](/worker/dotnet) worker which consumes votes and stores them in…
* A [Postgres](https://hub.docker.com/_/postgres/) or [TiDB](https://hub.docker.com/r/dockersamples/tidb/tags/) database backed by a Docker volume
* A [Node.js](/result) or [ASP.NET Core SignalR](/result/dotnet) webapp which shows the results of the voting in real time


Note
----

The voting application only accepts one vote per client. It does not register votes if a vote has already been submitted from a client.
